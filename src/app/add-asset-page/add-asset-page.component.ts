import { Component, OnInit } from "@angular/core";
import { Asset } from "../shared/models/Asset.model";
import { Router, ActivatedRoute } from "@angular/router";
import { AssetService } from "../shared/services/asset.service";
import { NgForm } from "@angular/forms";
import { __param } from "tslib";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-add-asset-page",
  templateUrl: "./add-asset-page.component.html",
  styleUrls: ["./add-asset-page.component.css"]
})
export class AddAssetPageComponent implements OnInit {
  asset: Asset;
  assetId: string;
  constructor(
    private router : Router,
    private service: AssetService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.resetForm();
    this.activatedRoute.queryParams.subscribe(param => {
      this.assetId = param["assetId"];
    });
    if (this.assetId) {
      this.service.getAsset(this.assetId).subscribe(res => {
        var ass = res as Asset;
        ass.id = this.assetId;
        this.asset = ass;
      });
    }
  }

  onSubmit(form: NgForm) {
    let data = form.value;
    if (this.asset.id == null) {
      this.service.addAsset(data).then(() => {
        this.toastr.success("Added successfully", "Asset");
        this.router.navigate(['stockPage']);
      });
    } else {
      this.service.updateAsset(data).then(() => {
        this.toastr.success("Updated successfully", "Asset");
        this.router.navigate(['stockPage']);
      });
    }
    this.resetForm(form);
  }

  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.asset = {
      id: null,
      name: "",
      serialNumber: "",
      quantity: null,
      price: null,
      details: "",
      company: ""
    };
  }
}
