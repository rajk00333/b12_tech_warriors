export class AllocateAsset{
 public userId : string;
 public assetId : string;    
 public fromDate : Date;
 public tillDate : Date;
 public quantity : number;
 public purpose : string;
 public accessAllocation: string;
}

export class OverviewAllocatedAssets {
    public userEmail: string;
    public userName: string;
    public phoneNumber: number;
    public assetName: string;
    public serialNumber: string;    
    public fromDate : Date;
    public tillDate : Date;
    public quantity : number;
    public purpose : string;
    public accessAllocation : string;
}