import { Component, OnInit } from '@angular/core';
import { Asset } from '../shared/models/Asset.model';
import { AllocateAsset, OverviewAllocatedAssets } from '../shared/models/allocateAsset.model';
import { Router } from '@angular/router';
import { allocateAssetService } from '../shared/services/allocateAsset';
import { AssetService } from '../shared/services/asset.service';
import { UserDetails } from '../shared/models/user-details.model';
import { UserDetailService } from '../shared/services/user-detail.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-over-view-page',
  templateUrl: './over-view-page.component.html',
  styleUrls: ['./over-view-page.component.css']
})
export class OverViewPageComponent implements OnInit {

  private AssetService: AssetService;
  assets: Asset[];
  userDetails: UserDetails[];
  overviewAssets: OverviewAllocatedAssets[];
  private UserDetailService: UserDetailService;

  private allocateAssetService: allocateAssetService;
  allocatedAssets: AllocateAsset[];

  constructor(_AllocateAssetService: allocateAssetService, userDetailService: UserDetailService, _AssetService: AssetService,
    private router: Router) {
    this.AssetService = _AssetService;
    this.UserDetailService = userDetailService;
    this.allocateAssetService = _AllocateAssetService;
  }

  ngOnInit() {
    this.allocateAssetService.getAllocatedAssets().subscribe(allocatedAssets => {
      this.allocatedAssets = this.SpreadOut(allocatedAssets) as AllocateAsset[];
      this.UserDetailService.getUserDetails().subscribe(userDetails => {
        this.userDetails = this.SpreadOut(userDetails) as UserDetails[];
        this.AssetService.getAssets().subscribe(assets => {
         this.assets =  this.SpreadOut(assets) as Asset[];   
            this.mapValues();
        });
      });
    });
  }
  
  SpreadOut(data: any): any[] {
    return data.map(i => {
      return {
        id: i.payload.doc.id,
        ...i.payload.doc.data()
      }
    });
  }

  mapValues() {
    this.overviewAssets  =  this.allocatedAssets.map(alloc => {
      var user:UserDetails =  this.userDetails.filter(x => x.id == alloc.userId)[0];
      var asset = this.assets.filter(x => x.id == alloc.assetId)[0];
      // console.log('>ass', asset);
      // console.log('>user', user);
      return {
        userName : user.userName,
        userEmail: user.userEmail,
        phoneNumber: user.phoneNumber, 
        assetName: asset.name,
        serialNumber: asset.serialNumber,
        fromDate : alloc.fromDate,
        tillDate : alloc.tillDate,
        quantity : alloc.quantity,
        purpose : alloc.purpose,
        accessAllocation: alloc.accessAllocation
      } as OverviewAllocatedAssets
    });
    // console.log(this.overviewAssets);
  }
}